const changedColor = "rgb(255, 218, 185)";

let button = document.getElementById("run");
button.onclick = buildTable;

let form = document.getElementById("form");

let table = document.getElementById("table");

let firstColor = table.backgroundColor;

table.onclick = function(event) {
    let target = event.target;

    if (target.tagName != "TD") return;

    changeColor(target);
}

function buildTable() {
    let rows = form.rows.value;
    let columns = form.columns.value;

    clearTable(table);

    table.insertRow(-1);

    for (let i = 0; i < rows; i++) {
        let newTr = document.createElement("tr");
        table.appendChild(newTr);

        for (let j = 0; j < columns; j++) {
            let newTd = document.createElement("td");
            let newValue = String(i+1) + String(j+1);
            newTd.append(newValue);
            newTr.appendChild(newTd);
        }
    }
}

function clearTable(table) {
    while (table.rows.length > 0) {
        table.deleteRow(0);
    }
}

function changeColor(td) {
    if (td.style.backgroundColor == changedColor) {
        td.style.backgroundColor = "transparent";
    }
    else {
        td.style.backgroundColor = changedColor;
    }
}

